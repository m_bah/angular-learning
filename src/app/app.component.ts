import {Component, OnDestroy, OnInit} from '@angular/core';
import {Observable, interval, Subscription} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  seconde: number;
  counterSubscription: Subscription;
  constructor() {
  }
  ngOnInit() {
    const counter = interval(1000);
    this.counterSubscription = counter.subscribe(
      (value) => {
        this.seconde = value;
      }
    );
  /**  counter.subscribe(
      (value) => {
        this.seconde = value;
      },
      (error: any) => {
        console.log('Une erreur à été rencontrée !');
      }, () => {
        console.log('Observable complétée !');
      }
    ); */
  }

  ngOnDestroy() {
    this.counterSubscription.unsubscribe();
  }

}
